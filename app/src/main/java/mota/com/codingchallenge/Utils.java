package mota.com.codingchallenge;

import android.util.Log;

import java.util.*;

public class Utils {
    private Utils() { }


    public static int solution(String s) {
        // write your code in Java SE 8
        String[] arr = s.split("!|\\.|\\?");
        int count =0;

        for(int i=0;i<arr.length;i++) {
            if(arr[i].length()>0) {
                String[] temp = arr[i].split(" ");
                temp =removeEmpty(temp);
                for(int j=0;j<temp.length;j++) {
                    Log.i("Printing","Mota:" + temp[j]);
                }

                if(temp.length>count) count = temp.length;
            }
        }
        return count;
    }

    private static ArrayList fillTheList(int n, ArrayList list) {
        for(int i=1;i<=n/2;i++) {
            list.add(i);
            list.add(-1*i);
        }
        return list;
    }

    private static int[] toIntArr(ArrayList<Integer> list){
        int[] arr = new int[list.size()];
        for(int i = 0;i < arr.length;i++)
            arr[i] = list.get(i);
        return arr;
    }

    private static String[] removeEmpty(String[] arr) {
        ArrayList<String> list = new ArrayList<>();
        for(int i=0;i<arr.length;i++) {
            if(arr[i].length()>0) {
                list.add(arr[i]);
            }
        }
        return toStringArray(list);
    }

    private static String[] toStringArraay(List<String> list){
        String[] ret = new String[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    // Utility
    private static int countOfUniqueElements(int[] a) {
        int types=1;
        Arrays.sort(a);
        for(int i=1;i<a.length;i++) {
            if(a[i] != a[i-1]) {
                types+=1;
            }
        }
        return types;
    }

    private static boolean hasEachIntUpto(TreeSet set, int value) {
        NavigableSet<Integer> headSet = set.headSet(value, true);
        return headSet.size() == value;
    }

    private int getSteps(int dist, int size) {
        Double steps = Math.ceil(dist/size);
        return steps.intValue();
    }

    private boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n%2==0) return false;
        //if not, then just check the odds
        for(int i=3;i*i<=n;i+=2) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    private static int countOfSub(String b, String sub) {
        String temp = b.replace(sub, "");
        return (b.length() - temp.length()) / sub.length();
    }

    private static boolean hasValue(int value, int[] arr) {
        for(int i=0;i<arr.length;i++) {
            if(arr[i] == value) return true;
        }
        return false;
    }

    private static int[] moveOneStepRight(int[] a) {
        ArrayList shifted = new ArrayList();
        shifted.add(a[a.length-1]);
        for(int i=0;i<a.length-1;i++) {
            shifted.add(a[i]);
        }
        return toIntArray(shifted);
    }

    private static String removeLeadingZeroes(String s) {
        StringBuilder sb = new StringBuilder(s);
        while (sb.length() > 0 && sb.charAt(0) == '0') {
            sb.deleteCharAt(0);
        }
        return sb.toString();
    }

    private static String removeTrailingZeroes(String s) {
        StringBuilder sb = new StringBuilder(s);
        while (sb.length() > 0 && sb.charAt(sb.length() - 1) == '0') {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    private static int[] onlyPositives(int[] numArray) {
        ArrayList pos = new ArrayList<>();
        for(int i=0;i<numArray.length;i++) {
            if(numArray[i] >0)
                pos.add(numArray[i]);
        }
        return toIntArray(pos);
    }

    private static int addElements(int[] a, int index, boolean forward_direction) {
        int sum=0;
        if(forward_direction) {
            for(int i=index;i<a.length;i++) {
                sum+=a[i];
            }
        } else {
            for(int i=0;i<index;i++) {
                sum+=a[i];
            }
        }
        return sum;
    }

    // Conversion
    private static HashMap<Integer, Integer> frequencyArray(int[] a) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++) {
            if(map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }

    private static HashMap<Character, Integer> frequencyArray(char[] a) {
        HashMap<Character, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++) {
            if(map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }

    private static HashMap<String, Integer> frequencyArray(String[] a) {
        HashMap<String, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++) {
            if(map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }

    private static int[] arrayToSet(int[] a) {
        Set<Integer> set = new HashSet<>();
        for(int i=0;i<a.length;i++){
            set.add(a[i]);
        }

        int[] array = new int[set.size()];
        int index = 0;
        for(Integer i : set){
            array[index++] = i;
        }
        return array;
    }

    private static int[] toIntArray(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    private static long[] toLongArray(List<Long> list){
        long[] ret = new long[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    private static ArrayList extractToList(int[] a, int index) {
        ArrayList list = new ArrayList();
        for(int i=0;i<a.length && i<=index;i++)
            list.add(a[i]);
        return list;
    }

    private static String[] toStringArray(List<String> list){
        String[] ret = new String[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    private static int[] toIntArray(SortedSet<Integer> set) {
            int[] a = new int[set.size()];
            int i = 0;
            for (Integer val : set) a[i++] = val;
            return a;
    }

    private static String[] toStringArray(TreeSet<String> set) {
        return set.toArray(new String[set.size()]);
    }

    // Solutions to problems
    private static int maxProductTriplets(int[] a) {
        // write your code in Java SE 8
        Arrays.sort(a);
        int l=a.length-1;
        return Math.max(a[l]*a[l-1]*a[l-2],a[0]*a[1]*a[l]);
    }

    private static int passingCars(int[] a) {
        int passes = 0;
        int one_count = 0;
        for(int i=a.length-1;i>=0;i--) {
            if(a[i] == 1) {
                one_count += 1;
            } else {
                passes = passes + one_count;
            }
        }

        if(passes > 1000000000) {
            return -1;
        }
        return passes;
    }
}


