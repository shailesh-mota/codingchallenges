package mota.com.codingchallenge;

import android.app.Application;
import android.util.Log;
import mota.com.codingchallenge.picnic.Solution2;

public class MainApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        int[] c = {3,0,10,3,12,4,14,11,10,1};
        int[] d = {2,3,6,3,2,4,1,1};
        Log.i("Mota","Result: " + Solution2.performantSolution(c, d));

    }
}
