package mota.com.codingchallenge.hacker.rank;

import java.util.HashMap;
import java.util.Map;

public class ShareSubstring {
    static String twoStrings(String s1, String s2) {
        HashMap<Character, Integer> map1 = frequencyArray(s1.toCharArray());
        HashMap<Character, Integer> map2 = frequencyArray(s2.toCharArray());

        for(Map.Entry<Character, Integer> entry : map1.entrySet()) {
            Character key = entry.getKey();
            if (map2.containsKey(key)) {
                return "YES";
            }
        }
        return "NO";
    }

    private static HashMap<Character, Integer> frequencyArray(char[] a) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i]) + 1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }
}
