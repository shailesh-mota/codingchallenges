package mota.com.codingchallenge.hacker.rank;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class RansomNote {

    // Complete the checkMagazine function below.
    public static void checkMagazine(String[] magazine, String[] note) {
        HashMap<String, Integer> map1 = frequencyArray(magazine);
        HashMap<String, Integer> map2 = frequencyArray(note);
        boolean possible = true;
        for(Map.Entry<String, Integer> entry : map2.entrySet()) {
            if(map1.containsKey(entry.getKey())) { // word check
                Integer value = map1.get(entry.getKey());
                // freq. of word
                if(!(entry.getValue()  <= value)) {
                    possible = false;
                }
            } else {
                possible = false;
            }
        }
        
        System.out.print(possible? "Yes":"No");
    }

    private static HashMap<String, Integer> frequencyArray(String[] a) {
        HashMap<String, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++) {
            if(map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }
}
