package mota.com.codingchallenge.hacker.rank;

import android.util.Log;

public class PasswordChecker {
    private PasswordChecker() {}

    public static int minimumNumber(int n, String password) {
        // Return the minimum number of characters to make the password strong
        boolean check_len = password.length() >= 6;

        if (check_len) {
            return completeChecks(password);
        } else {
            int to_fill = 6 - password.length();
            int min = completeChecks(password);
            return (min >= to_fill) ? min : 6 - password.length();
        }
    }

    private static int completeChecks(String password) {
        int min = 0;
        boolean check_dcase = password.matches(".*\\d.*");
        boolean check_scase = password.matches(".*[!@#$%^&*()\\-+].*");
        boolean check_ucase = !password.equals(password.toLowerCase());
        boolean check_lcase = !password.equals(password.toUpperCase());

        if (!check_dcase) {
            ++min;
        }
        if (!check_scase) {
            ++min;
        }
        if (!check_ucase) {
            ++min;
        }
        if (!check_lcase) {
            ++min;
        }
        return min;
    }
}
