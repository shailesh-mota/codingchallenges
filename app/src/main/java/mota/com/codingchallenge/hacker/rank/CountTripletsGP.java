package mota.com.codingchallenge.hacker.rank;

import java.util.*;

//TODO fails scenario
//5 2
//1 2 1 2 4
// Out put : 3
// Position is important

public class CountTripletsGP {
    static long countTriplets(List<Long> arr, long r) {
        long[] a = toLongArray(arr);
        HashMap<Long, Integer> map = frequencyArray(a);
        long count = 0;
        for (Long key :  map.keySet()) {
            long count_1 = map.get(key);
            long i_1 = getNextGPItem(key,r);
            if(arr.contains(i_1)) {
                // get count of i_1
                long count_2  = map.get(i_1);
                long i_2 = getNextGPItem(i_1,r);
                if(arr.contains(i_2)) {
                    // get count of i_1, temp = temp + count
                    long count_3  = map.get(i_2);
                    count = count + count_1 * count_2 * count_3;
                }
            }
        }
        return count;
    }

    static long getNextGPItem(long current, long r) {
        return current*r;
    }

    private static HashMap<Long, Integer> frequencyArray(long[] a) {
        HashMap<Long, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++) {
            if(map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }

    private static long[] toLongArray(List<Long> list){
        long[] ret = new long[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }
}
