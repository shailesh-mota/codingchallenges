package mota.com.codingchallenge.hacker.rank;

import java.util.ArrayList;

public class ArrayLeftShift {
    // Complete the rotLeft function below.
    static int[] rotLeft(int[] a, int d) {
        if(d==a.length) {
            return a;

        } else if(d <a.length) {
            return normalLeftStep(a, d);
        } else {
            d= d%a.length;
            return normalLeftStep(a, d);
        }


    }

    static int[] normalLeftStep(int[] a, int d) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i=d-1;i<a.length;i++) {
            list.add(a[i]);
        }

        for(int i=0;i<d;i++) {
            list.add(a[i]);
        }
        return toIntArr(list);

    }

    static int[] toIntArr(ArrayList<Integer> list) {
        int[] arr = new int[list.size()];
        for (int i = 0; i < arr.length; i++)
            arr[i] = list.get(i);
        return arr;
    }
}
