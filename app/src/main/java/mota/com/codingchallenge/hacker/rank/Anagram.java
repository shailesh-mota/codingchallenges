package mota.com.codingchallenge.hacker.rank;

import java.util.HashMap;
import java.util.Map;

public class Anagram {
    // Complete the makeAnagram function below.
    static int makeAnagram(String a, String b) {
        int total_len = a.concat(b).length();
        HashMap<Character, Integer> map1 = frequencyArray(a.toCharArray());
        HashMap<Character, Integer> map2 = frequencyArray(b.toCharArray());

        for (Map.Entry<Character, Integer> entry : map1.entrySet()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            if (map2.containsKey(key)) {
                total_len = total_len - 2 * Integer.min(value, map2.get(key));
            }
        }
        return total_len;
    }

    private static HashMap<Character, Integer> frequencyArray(char[] a) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i]) + 1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }
}
