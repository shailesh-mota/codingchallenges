package mota.com.codingchallenge.hacker.rank;

import java.util.HashMap;
public class SherlockString {

    // TODO doesn't work for extremely large strings
    static String isValid(String s) {
        HashMap<Character, Integer> map = frequencyArray(s.toCharArray());
        HashMap<Character, Integer> copy = new HashMap<>();
        copy.putAll(map);
        if (isValid1(map) || isValid2(map) || isValid3(copy)) {
            return "YES";
        } else {
            return "NO";
        }
    }

    private static boolean isValid1(HashMap<Character, Integer> map) {
        Integer count = getFirstValue(map);

        for (Integer value : map.values()) {
            if (count != value)
                return false;
        }
        return true;
    }

    private static boolean isValid2(HashMap<Character, Integer> map) {
        map = removeMaxValueForAKey(map);
        return isValid1(map);
    }

    private static boolean isValid3(HashMap<Character, Integer> map) {
        map = removeMinValueForAKey(map);
        return isValid1(map);
    }

    private static HashMap<Character, Integer> removeMinValueForAKey(HashMap<Character, Integer> map) {
        Integer count = 1;
        for (HashMap.Entry<Character, Integer> entry : map.entrySet()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            if (value == count) {
                map.remove(key);
                break;
            }
        }
        return map;
    }

    private static HashMap<Character, Integer> removeMaxValueForAKey(HashMap<Character, Integer> map) {
        Integer count = -1;
        Character keyF = 'a';
        for (HashMap.Entry<Character, Integer> entry : map.entrySet()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            if (value > count) {
                count = value;
                keyF = key;
            }
        }

        map.replace(keyF, count - 1);

        return map;
    }

    private static Integer getFirstValue(HashMap<Character, Integer> map) {
        HashMap.Entry<Character, Integer> entry = map.entrySet().iterator().next();
        return entry.getValue();
    }

    // Conversion
    private static HashMap<Character, Integer> frequencyArray(char[] a) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            if (map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i]) + 1);
            } else {
                map.put(a[i], 1);
            }
        }
        return map;
    }
}