package mota.com.codingchallenge.hacker.rank;

public class HackerRankInString {
    // Complete the hackerrankInString function below.
    static String hackerrankInString(String s) {
        return s.matches(".*h+.*a+.*c+.*k+.*e+.*r+.*r+.*a+.*n+.*k+.*") ? "YES" : "NO";
    }
}
