package mota.com.codingchallenge.RegexPractice;

public class StringChecks {
    private StringChecks() {}
    public static boolean stringHasAnyDigit(String text) {
        return text.matches(".*[0-9].*");
    }

    public static boolean stringHasAnySpecialChars(String text) {
        return text.matches(".*[!@#$%^&*()-+].*");
    }

    public static boolean stringHasAnyLowerCaseChar(String text) {
        return text.matches(".*[a-z].*");
    }

    public static boolean stringHasAnyUpperCaseChar(String text) {
        return text.matches(".*[A-Z].*");
    }
}
