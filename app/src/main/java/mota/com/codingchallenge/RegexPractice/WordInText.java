package mota.com.codingchallenge.RegexPractice;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

// case sensitive
public class WordInText {
    private WordInText(){}

    public static int countOfWord(String text, String word) {
        Map<String, Integer> map = getWords(text);
        return map.containsKey(word) ? map.get(word) : 0;
    }

    static Map<String, Integer> getWords(String text) {
        String[] words = text.split("([\\W\\s]+)");
        Map<String, Integer> counts = new HashMap<>();
        for (String word: words) {
            if (counts.containsKey(word)) {
                counts.put(word, counts.get(word) + 1);
            } else {
                counts.put(word, 1);
            }
        }
        return counts;
    }
}
