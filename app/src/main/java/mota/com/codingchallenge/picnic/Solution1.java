package mota.com.codingchallenge.picnic;

import java.util.ArrayList;
import java.util.List;

public class Solution1 {

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

        public int solution(String s) {
            // write your code in Java SE 8
            String[] arr = s.split("!|\\.|\\?");
            int count =0;
            for(int i=0;i<arr.length;i++) {
                if(arr[i].length()>0) {
                    String[] temp = arr[i].split(" ");
                    temp =removeEmpty(temp);
                    if(temp.length>count) count = temp.length;
                }
            }
            return count;
        }

        private static String[] removeEmpty(String[] arr) {
            ArrayList<String> list = new ArrayList<>();
            for(int i=0;i<arr.length;i++) {
                if(arr[i].length()>0) {
                    list.add(arr[i]);
                }
            }
            return toStringArray(list);
        }

        private static String[] toStringArray(List<String> list){
            String[] ret = new String[list.size()];
            for(int i = 0;i < ret.length;i++)
                ret[i] = list.get(i);
            return ret;
        }
}
