package mota.com.codingchallenge.picnic;

import java.util.*;

// TODO PERFORMANCE!!!
public class Solution2 {
    int solution(int[] A, int[] B) {
        int n = A.length;
        int m = B.length;;
        Arrays.sort(A);
        Arrays.sort(B);
        int i = 0;
        for (int k = 0; k < n; k++) {
            while (i < m && B[i] < A[k])
                i += 1;
            if (A[k] == B[i])
                return A[k];
        }
        return -1;
    }

    public static int performantSolution(int[] a, int[] b) {
        TreeSet<Integer> setA = new TreeSet<>();
        TreeSet<Integer> setB = new TreeSet<>();
        for(int i=0;i<a.length;i++) setA.add(a[i]);
        for(int i=0;i<b.length;i++) setB.add(b[i]);

        // c = a+b
        ArrayList listC = new ArrayList(setA);
        listC.addAll(setB);
        int[] c = toIntArray(listC);
        Arrays.sort(c);
        return getFirstDup(c);

    }

    private static int getFirstDup(int[] c) {
        for(int i=0;i<c.length-1;i++) {
            if(c[i] == c[i+1]) {
                return c[i];
            }
        }
        return -1;
    }

    private static int[] toIntArray(TreeSet<Integer> set) {
        int[] a = new int[set.size()];
        int i = 0;
        for (Integer val : set) a[i++] = val;
        return a;
    }

    private static int[] toIntArray(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }
}
