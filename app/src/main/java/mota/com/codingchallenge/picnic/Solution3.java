package mota.com.codingchallenge.picnic;

// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

public class Solution3 {
    public int[] solution(int n) {
        // write your code in Java SE 8
        // 1<=n<=100
        ArrayList list = new ArrayList();
        if(n%2 == 0) { // even
            list = fillTheList(n, list);
        } else { // odd
            list.add(0);
            list = fillTheList(n-1, list);
        }
        return toIntArray(list);
    }

    private static ArrayList fillTheList(int n, ArrayList list) {
        for(int i=1;i<=n/2;i++) {
            list.add(i);
            list.add(-1*i);
        }
        return list;
    }

    private static int[] toIntArray(ArrayList<Integer> list){
        int[] arr = new int[list.size()];
        for(int i = 0;i < arr.length;i++)
            arr[i] = list.get(i);
        return arr;
    }

}
